# ---------------------------------------------------------------------
# Zone de test:
SELECT * FROM Product;

# Créer un produit “Truc”, avec comme description “Lorem Ipsum” comme 
# image “https://picsum.photos/200”, qui coute 200 €.
INSERT INTO `Product` (`name`, `description`, `image`, `price`) 
VALUES("Truc", "Lorem Ipsum", "https://picsum.photos/200", 200);

INSERT INTO `Product` (`name`, `description`, `image`, `price`) 
VALUES("Chose", "Lorem bestial", "https://picsum.photos/300", 450);

# Créer un produit “Bidule”, avec comme description “Lorem merol” comme 
# image “https://picsum.photos/100”, qui coute 500 €.
INSERT INTO `Product` (`name`, `description`, `image`, `price`) 
VALUES("Bidule", "Lorem merol", "https://picsum.photos/100", 500);

# Récupérer la liste des produits et vérifier le prix de Truc et de Bidule.
SELECT `product_id`, `name`, `description`, `image`, `price`, `available` FROM Product;
SELECT `price` FROM Product
WHERE `name` = "Truc";
SELECT `price` FROM Product
WHERE `name` = "Bidule";

# Récupérer Truc à partir de son id et vérifier sa description et son image.
SELECT `description`, `image` FROM Product
WHERE `product_id` in (SELECT `product_id` FROM Product WHERE `name` = "Truc");


SELECT * FROM `Order`;

# Créer une nouvelle commande.
INSERT INTO `Order` 
VALUES();

SELECT * FROM `Order_lines`;

# Ajouter 3 Trucs à la commande.
INSERT INTO `Order_lines` (`order_id`, `product_id`, `qt`) 
VALUES(5000, 1, 3);

# Ajouter 1 Bidules à la commande.
INSERT INTO `Order_lines` (`order_id`, `product_id`, `qt`) 
VALUES(5000, 2, 1);


SELECT * FROM `Client`;

# Créer le client “Bob” avec comme mail “bob@gmail.com”.
INSERT INTO `Client` (`name`, `email`) 
VALUES("Bob", "bob@gmail.com");

INSERT INTO `Client` (`name`, `email`) 
VALUES("Fred", "fred@gmail.com");

# Mettre à jour Bob pour ajouter son numéro de téléphone “0123456789”.
UPDATE `Client` SET `phone` = "0123456789"
WHERE `name` = "Bob";

UPDATE `Client` SET `phone` = "0123456789"
WHERE `name` = "Fred";

SELECT * FROM `Order`;
SELECT * FROM `Client`;

# Essayer de passer la commande dans l’état suivant (vérifier qu’il y a une erreur).
# Erreur uniquement en mode requête Http.
UPDATE `Order` SET `order_state` = "validated"
WHERE `order_id` = 5000;

# Associer Bob à la commande
UPDATE `Order` SET `client` = 1
WHERE `order_id` = 5000;

UPDATE `Order` SET `client` = 2
WHERE `order_id` = 5001;


SELECT * FROM `Adresse`;
SELECT * FROM `Order`;
SELECT * FROM `Client`;
SELECT * FROM `Client_adress`

# Créer l’adresse “12 street St, 12345 Schenectady, New York, US”
INSERT INTO `Adresse` (`street`, `city`, `zipcode`, `state`, `country`) 
VALUES("12 street St", "Schenectady", 12345, "New York", "US");

# Essayer d’associer cette adresse comme facturation à la commande.
UPDATE `Order` SET `biling_adress` = 1
WHERE `order_id` = 5000;

# Associer cette adresse à Bob.
INSERT INTO `Client_adress` (`client_id`, `adress_id`)
VALUES(1, 1)

UPDATE `Client` SET `default_adress` = 2
WHERE `name` = "Bob";

INSERT INTO `Client_adress` (`client_id`, `adress_id`)
VALUES(2, 1)

UPDATE `Client` SET `default_adress` = 1
WHERE `name` = "Fred";

# Associer cette adresse comme facturation à la commande.
UPDATE `Order` SET `delevery_adress` = 1
WHERE `order_id` = 5000;


SELECT * FROM `Adresse`;
SELECT * FROM `Order`;
SELECT * FROM `Client`;
SELECT * FROM `Client_adress`

# Créer l’adresse “12 boulevard de Strasbourg, 31000, Toulouse, OC, France”
INSERT INTO `Adresse` (`street`, `city`, `zipcode`, `state`, `country`) 
VALUES("12 boulevard de Strasbourg", "Toulouse", 31000, "OC ", "France");

INSERT INTO `Adresse` (`street`, `city`, `zipcode`, `state`, `country`) 
VALUES("3 rue claude cornac", "Toulouse", 31140, "OC ", "France");

# Associer cette adresse à Bob.
INSERT INTO `Client_adress` (`client_id`, `adress_id`)
VALUES(1, 2)

# Associer cette adresse comme adresse de livraison.
UPDATE `Order` SET `delevery_adress` = 2
WHERE `order_id` = 5000;

# Essayer de passer la commande dans l’état suivant (vérifier qu’il y a une erreur).
UPDATE `Order` SET `order_state` = "validated"
WHERE `order_id` = 5000;

# Associer le moyen de paiement “Card” à la commande.
UPDATE `Order` SET `payment_method` = "credit_card"
WHERE `order_id` = 5000;

# Passer la commande dans l’état suivant.
UPDATE `Order` SET `order_state` = "validated"
WHERE `order_id` = 5000;

# Passer la commande dans l’état suivant.
UPDATE `Order` SET `order_state` = "sent"
WHERE `order_id` = 5000;

# Vérifier que la commande est dans l’état “sent”.
SELECT `order_state` FROM `Order`
WHERE `order_id` = 5000;

# Récupérer la liste des adresses de Bob.
SELECT `Client`.`name`, `Client_adress`.`client_id`, `Client_adress`.`adress_id`, 
`Adresse`.`street`, `Adresse`.`city`, `Adresse`.`country`, 
`Adresse`.`zipcode`, `Adresse`.`state`
FROM `Client_adress`
JOIN data_at_checkout.`Client` ON `Client`.`client_id` = `Client_adress`.`client_id`
JOIN data_at_checkout.`Adresse` ON `Adresse`.`adress_id` = `Client_adress`.`adress_id`
WHERE `Client_adress`.`client_id` IN (
    SELECT `client_id` FROM `Client`
    WHERE `name` = "Bob");


# Récupérer la liste des commandes de Bob.
SELECT `Order`.`order_id`, `Client`.`name` FROM `Order`
JOIN data_at_checkout.`Client` ON `Client`.`client_id` = `Order`.`client`
WHERE `Client`.`client_id` IN (
    SELECT `client_id` FROM `Client`
    WHERE `name` = "Bob");