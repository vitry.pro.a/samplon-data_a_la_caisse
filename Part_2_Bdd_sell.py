import mysql.connector
from fastapi import FastAPI
from model_dto import Client, Adress, Client_adress, Order, Product, Order_lines, DictToObject


# Initialisation des connections API / SQL:----------------------------------
# Create Connection SQL
mydb = mysql.connector.connect(
  host="localhost",
  user="antony",
  password="choupette",
  database="data_at_checkout"
)

# Create Connection API
app = FastAPI(debug=True)


# Création des fonctions:----------------------------------------------------
def cursor_bdd(p: bool, sql: str, val: list = None):
    mycursor = mydb.cursor(dictionary=True, buffered=True)
    if val != None:
        mycursor.execute(sql, val)
    else:
        mycursor.execute(sql)
    if p is True:
        mydb.commit()
    return mycursor

def sort_dict_order_line(cursor):
    dico_client = {}
    liste_cmd = []
    dico_cmd = {}
    result = []
    cond_client = ["order_id", "client_name", "delevery_id", "delevery_street", 
                "delevery_city", "delevery_zipcode", "delevery_state",
                "delevery_country", "billing_id", "billing_street", 
                "billing_city", "billing_zipcode", "billing_state",
                "billing_country"]
    cond_product = ["product_id", "product_name", "qt", "description", 
                    "image", "price"]
    for dico in cursor:
        for item in dico.items():
            if item[0] in cond_client:
                dico_client[item[0]] = item[1]
            elif item[0] in cond_product:
                dico_cmd[item[0]] = item[1]
        liste_cmd.append(dico_cmd)
    result.append(dico_client), result.append(liste_cmd)
    return result

# ------------------------------------------
# ACCUEIL --- Création des fonctions API:  |
# ------------------------------------------

# / GET Accueil Home de l'API :  CHECK OK
@app.get("/")
def read_root():
    """ Page d'accueil sur le test d'une API """
    return {"Home": "Page d'accueil sur le test d'une API"}

# ------------------------------------------
# PRODUCT --- Création des fonctions API:  |
# ------------------------------------------

# /product - GET - Pas de Body : CHECK OK - avec fonction
@app.get("/product")
def read_product() -> list:
    """ Liste des produits en JSON """
    request_sql = """
    SELECT `product_id`, `name`, `description`, `image`, `price`, `available` 
    FROM Product;"""
    cursor = cursor_bdd(False,
                        request_sql)
    return cursor.fetchall()

# /product - GET - Pas de Body : CHECK OK - sans fonction
# @app.get("/product")
# def read_product():
#     """ Liste des produits en JSON """
#     sql = "SELECT `product_id`, `name`, `description`, `image`, `price`, `available` FROM Product;"
#     mycursor = mydb.cursor(dictionary=True)
#     mycursor.execute(sql)
#     liste_de_product = mycursor.fetchall()
#     return liste_de_product


# /product/<id> - GET - id: id du produit : CHECK OK - avec fonction
@app.get("/product/{id}")
def read_product_by_id(id: int) -> dict:
    """ Un produit en JSON """
    request_sql = """
    SELECT `product_id`, `name`, `description`, `image`, `price`, `available` 
    FROM Product 
    WHERE `product_id` = %s;"""
    cursor = cursor_bdd(True,
                        request_sql,
                        [id])
    return cursor.fetchone()

# /product/<id> - POST - body: un produit en JSON / id: id du produit : CHECK OK - sans fonction
@app.post("/product/{id}")
def update_product_by_id(id: int, product: Product) -> Product:
    """ Mettre à jour le produit """
    mycursor = mydb.cursor(dictionary=True, buffered=True)
    # Récupération de la description de product
    desc = product.description
    # Récupération des données de la database
    request_sql = """
    SELECT `product_id`, `name`, `description`, `image`, `price`, `available` 
    FROM Product 
    WHERE `product_id` = %s;"""
    mycursor.execute(request_sql, [id])
    # Récupération du dictionnaire du curseur
    dico_bdd = mycursor.fetchone()
    # Modification de l'ancienne description dans le dico_bdd
    dico_bdd["description"] = desc
    # Création d'un nouveau objet à partir du dico
    bdd_object = DictToObject(dico_bdd)
    # Mets à jour la database à partir du Body
    request_sql = "UPDATE `Product` SET `description`= %s WHERE `product_id` = %s;"
    mycursor.execute(request_sql, [bdd_object.description, bdd_object.product_id])
    mydb.commit()
    return bdd_object

# /product - POST - body: un produit en JSON : CHECK OK - avec fonction
@app.post("/product")
def create_product(product: Product) -> Product:
    """ Le même produit en JSON avec son id """
    request_sql = "INSERT INTO Product (`name`,`price`) VALUES (%s, %s);"
    cursor = cursor_bdd(True,
           request_sql, 
           [product.name, product.price])
    product.product_id = cursor.lastrowid
    return product



# /product/<id> - DELETE - id: id du produit : CHECK OK
@app.delete("/product/{id}")
async def delete_product_by_id(id: int) -> dict:
    """ Supprime le produit """
    request_sql = "DELETE FROM `Product` WHERE `product_id` = %s;"
    cursor = cursor_bdd(True,
                        request_sql,
                        [id])
    return {"ID": id, "Delete": "Le produit a bien été supprimé."}

# ------------------------------------------
# CLIENT --- Création des fonctions API:   |
# ------------------------------------------

# /client - GET - Pas de Body : CHECK OK
@app.get("/client")
def read_client() -> list:
    """ Liste des client en JSON """
    request_sql = """
    SELECT `client_id`, `name`, `email`, `phone`, `default_adress` 
    FROM `Client`;"""
    cursor = cursor_bdd(False,
                        request_sql)
    return cursor.fetchall()

# /client/<id> - GET - id: id du client : CHECK OK
@app.get("/client/{id}")
def read_client_by_id(id: int) -> dict:
    """ Un client en JSON """
    request_sql = """
    SELECT `client_id`, `name`, `email`, `phone`, `default_adress` 
    FROM `Client` 
    WHERE `client_id` = %s;"""
    cursor = cursor_bdd(True,
                        request_sql,
                        [id])
    return cursor.fetchone()

# /client - POST - body: un client en JSON : CHECK OK
@app.post("/client")
def create_client(client: Client) -> Client:
    """ Le même client en JSON avec son id """
    request_sql = "INSERT INTO Client (`name`,`email`) VALUES (%s, %s);"
    cursor = cursor_bdd(True,
           request_sql, 
           [client.name, client.email])
    client.client_id = cursor.lastrowid
    return client

# /client/<id> - POST - body: un client en JSON / id: id du client : CHECK OK
@app.post("/client/{id}")
def create_client_by_id(id: int, client: Client) -> Client:
    """ Mettre à jour le client """
    mycursor = mydb.cursor(dictionary=True, buffered=True)
    # Récupération des champs à mettre à jour
    phone = client.phone
    default_adress = client.default_adress
    # Récupération des données de la database
    request_sql = """
    SELECT `client_id`, `name`, `email`, `phone`, `default_adress` 
    FROM `Client` 
    WHERE `client_id` = %s;"""
    mycursor.execute(request_sql, [id])
    # Récupération du dictionnaire du curseur
    dico_bdd = mycursor.fetchone()
    # Modification de l'ancienne description dans le dico_bdd
    dico_bdd["phone"] = phone
    dico_bdd["default_adress"] = default_adress
    # Création d'un nouveau objet à partir du dico
    bdd_object = DictToObject(dico_bdd)
    # Mets à jour la database à partir du Body
    request_sql = """
    UPDATE `Client` SET `phone` = %s, `default_adress` = %s 
    WHERE `client_id` = %s;"""
    val = [bdd_object.phone, bdd_object.default_adress, bdd_object.client_id]
    mycursor.execute(request_sql, val)
    mydb.commit()
    return bdd_object

# /client/<id>/adresses - GET - body: id du client : CHECK OK
@app.get("/client/{id}/adresses")
def read_adresses_client_by_id(id: int):
    """ Retourne la liste des adresses associées à un client """
    request_sql = """
    SELECT `Client`.`name`, `Client_adress`.`client_id`, `Client_adress`.`adress_id`, 
    `Adresse`.`street`, `Adresse`.`city`, `Adresse`.`country`, 
    `Adresse`.`zipcode`, `Adresse`.`state`
    FROM `Client_adress`
    JOIN data_at_checkout.`Client` ON `Client`.`client_id` = `Client_adress`.`client_id`
    JOIN data_at_checkout.`Adresse` ON `Adresse`.`adress_id` = `Client_adress`.`adress_id`
    WHERE `Client_adress`.`client_id` = %s;
    """
    cursor = cursor_bdd(False,
                        request_sql,
                        [id])
    return cursor.fetchall()

# /client/<id> - DELETE - id: id du client : CHECK OK
@app.delete("/client/{id}")
def delete_client_by_id(id: int) -> dict:
    """ Supprime le client"""
    request_sql = "DELETE FROM `Client` WHERE `client_id` = %s;"
    try:
        cursor = cursor_bdd(True,
                    request_sql,
                    [id])
        return {"ID": id, "Delete": "Le client a bien été supprimé."}
    except mysql.connector.errors.IntegrityError:
        print("Erreur gérer correctement")
        return {"Erreur": "Le client ne peut pas être supprimé.",
                "Type": "1451 (23000): Cannot delete or update a parent row",
                "Consigne": "Retirer l'affectation du client sur la commande d'abord !!"}

# ------------------------------------------
# ORDER --- Création des fonctions API:    |
# ------------------------------------------

# /order - PUT - Pas de Body : CHECH OK
@app.put("/order")
def create_order() -> dict:
    """ Crée une nouvelle commande et retourne son id """
    request_sql = "INSERT INTO `Order` VALUES ();"
    cursor = cursor_bdd(True,
                        request_sql)
    request_sql = "SELECT LAST_INSERT_ID();"
    cursor = cursor_bdd(False,
                        request_sql)
    return {"ID commande": cursor.fetchone()["LAST_INSERT_ID()"]}

# /order/<id> - GET - body: CHECH OK
# id de la commande :
@app.get("/order/{id}")
def read_order_by_id(id: int) -> list:
    """ Retourne la commande avec toutes ses lignes, les adresses et le client. """
    # Requête Order
    request_order = """
    SELECT `order_id`, `client`, `delevery_adress`, `biling_adress`, `payment_method`, `order_state` 
    FROM `Order`
    WHERE `order_id` = %s;
        """
    cursor = cursor_bdd(True,
                        request_order,
                        [id])
    dico_order: dict = cursor.fetchone()
    # Sélection de la variable pour la requète suivante
    print(dico_order)
    print()
    client = dico_order["client"]
    delevery = dico_order["delevery_adress"]
    billing = dico_order["biling_adress"]
    # Condition: Assignation client
    if client != None:
        # Requête client
        request_client = """
        SELECT `client_id`, `name`, `email`, `phone`, `default_adress`
        FROM `Client`
        WHERE `client_id` = %s;
            """
        cursor = cursor_bdd(True,
                            request_client,
                            [client])
        dico_client: dict = cursor.fetchone()
        # Sélection de la variable pour la requète suivante
        print(dico_client)
        dico_order["client"] = dico_client
        print(dico_order)
        print()

        # Condition: Assignation delevery
        if delevery != None:
            # Requête delevery
            request_delevery = """
            SELECT `adress_id`, `street`, `city`, `zipcode`, `state`, `country` FROM `Adresse`
            WHERE `adress_id` = %s;
                """
            cursor = cursor_bdd(True,
                                request_delevery,
                                [delevery])
            dico_delevery: dict = cursor.fetchone()
            # Sélection de la variable pour la requète suivante
            print(dico_delevery)
            dico_order["delevery_adress"] = dico_delevery
            print(dico_order)
            print()

            # Condition: Assignation billing
            if delevery != None:
                # Requête billing
                request_billing = """
                SELECT `adress_id`, `street`, `city`, `zipcode`, `state`, `country` FROM `Adresse`
                WHERE `adress_id` = %s;
                    """
                cursor = cursor_bdd(True,
                                    request_billing,
                                    [billing])
                dico_billing: dict = cursor.fetchone()
                # Sélection de la variable pour la requète suivante
                print(dico_billing)
                dico_order["delevery_billing"] = dico_billing
                print(dico_order)
                print()

                # Requête Order_lines
                request_order_lines = """
                SELECT `order_id`, `product_id`, `qt` FROM `Order_lines`
                WHERE `order_id` = %s;
                    """
                cursor = cursor_bdd(True,
                                    request_order_lines,
                                    [id])
                dico_order_lines: list = cursor.fetchall()
                # Sélection de la variable pour la requète suivante
                print(dico_order_lines)
                print()
 
                # Condition: Assignation billing
                if dico_order_lines != None:
                    liste_dico_product = []
                    for i, product in enumerate(dico_order_lines):
                        # Requête Product
                        request_product = """
                        SELECT `product_id`, `name`, `description`, `image`, `price`, `available` 
                        FROM `Product`
                        WHERE `product_id` = %s;
                            """
                        cursor = cursor_bdd(True,
                                            request_product,
                                            [product["product_id"]])
                        dico_product: dict = cursor.fetchone()
                        # Sélection de la variable pour la requète suivante
                        print(dico_product)
                        liste_dico_product.append(dico_product)
                        dico_order_lines[i]["product_id"] = dico_product
                        print()

                    return [dico_order, dico_order_lines]
                else:
                    return [dico_order,
                            {"Erreur": "Pas de produit dans la commande"}]
            else:
                return [dico_order,
                        {"Erreur": "Pas d'adresse billing assigné"}]
        else:
            return [dico_order,
                    {"Erreur": "Pas d'adresse delevery assigné"}]
    else:
        return [dico_order,
                {"Erreur": "Pas de client assigné"}]

# /order - GET - pas de body : CHECH OK
@app.get("/order")
def read_order() -> list:
    """ Retourne la liste des commandes """
    request_sql = """
    SELECT `order_id`, `client`, `delevery_adress`, `biling_adress`, `order_state`, 
    `payment_method` FROM `Order`;"""
    cursor = cursor_bdd(False,
                        request_sql)
    return cursor.fetchall()

# /order/byClient/<id> - GET - body: CHECH OK
# id: id du client
@app.get("/order/byClient/{id}")
def read_order_by_client(id: int) -> list:
    """ Retourne la liste des ids des commandes du client. """
    request_sql = """
    SELECT `client`, `order_id` FROM `Order`
    WHERE `client` = %s;"""
    cursor = cursor_bdd(True,
                        request_sql,
                        [id])
    return cursor.fetchall()

# /order/<id>/<product_id>/<qt> - PUT - body: CHECH OK
# id: id de la commande / product_id: id du produit / qt: quantité de produit
@app.put("/order/{id}/{product_id}/{qt}")
def update_product_by_id(id: int, product_id: int, qt: int) -> Order_lines:
    """ Ajoute ou met à jour une ligne de commande. """
    mycursor = mydb.cursor(dictionary=True, buffered=True)
    # Récupération des données de la database
    request_sql = """
    SELECT `order_id`, `product_id`, `qt`
    FROM `Order_lines`
    WHERE `order_id` = %s and `product_id` = %s;"""
    mycursor.execute(request_sql, [id, product_id])
    # Récupération du dictionnaire du curseur
    dico_bdd = mycursor.fetchone()
    if dico_bdd == None:
        print("Pas de produit: {product_id}")
        dico_bdd = dict(order_id = id, product_id = product_id, qt = qt)
        # Création d'un nouveau objet à partir du dico
        bdd_object = DictToObject(dico_bdd)
        # Mets à jour la database à partir du Body
        request_sql = """
        INSERT INTO `Order_lines` (`order_id`,`product_id`, `qt`) 
        VALUES (%s, %s, %s)"""
        val = [bdd_object.order_id, bdd_object.product_id, bdd_object.qt]
        mycursor.execute(request_sql, val)
        mydb.commit()
        return bdd_object
    else:
        # print(f"""La commande: {dico_bdd['order_id']}, produit existant: {dico_bdd['product_id']} et de quantité: {dico_bdd['qt']}""")
        # Modification de l'ancienne description dans le dico_bdd
        dico_bdd["order_id"] = id
        dico_bdd["product_id"] = product_id
        dico_bdd["qt"] = qt
        # Création d'un nouveau objet à partir du dico
        bdd_object = DictToObject(dico_bdd)
        # print("Classe:",bdd_object.order_id, bdd_object.product_id, bdd_object.qt)
        # Mets à jour la database à partir du Body
        request_sql = """
        UPDATE `Order_lines` SET `qt` = %s
        WHERE `order_id` = %s and `product_id` = %s;"""
        val = [bdd_object.qt, bdd_object.order_id, bdd_object.product_id]
        mycursor.execute(request_sql, val)
        mydb.commit()
        return bdd_object

# /order/<id>/<client_id> - PUT - body :  CHECH OK
# id: id de la commande / client_id: id du client
@app.put("/order/{id}/{client_id}")
def update_commande_by_client_id(id: int, client_id: int) -> dict:
    """ Assigne une commande à un client. 
Attention! une fois qu'une commande est attribuée à un client on ne peut 
plus modifier cette relation """
    print(id, client_id)
    request_sql_1 = """
    SELECT `client`, `order_id` FROM `Order`
    WHERE order_id = %s;"""
    cursor = cursor_bdd(False,
                        request_sql_1,
                        [id])
    bdd_dico = cursor.fetchone()
    print(bdd_dico)
    if bdd_dico != None:
        if bdd_dico["client"] != None:
            if bdd_dico["client"] == client_id:
                # si order_id existe avec client identique à id
                    # on renvoi un dico erreur (client déjà assigné)
                print(bdd_dico["client"], "==", client_id)
                return {"Commande": id, 
                        "Client": f"Le client {bdd_dico['client']} est déjà assigné à la commande."}
            elif bdd_dico["client"] != client_id:
                # si order_id existe avec client différent à id
                    # on renvoir un dico erreur (client non modifiable)
                print(bdd_dico["client"], "!=", client_id)
                return {"Commande": id, 
                        "Client": f"Le client {bdd_dico['client']} est non modifiable."}
        else:
            # si order_id existe mais sans id
                    # on insert le client
            request_sql_2 = """
            UPDATE `Order` SET `client` = %s 
            WHERE `order_id` = %s;"""
            cursor_bdd(True, request_sql_2, [client_id, id])
            return {"Commande": id, 
                    "Client": f"Le client {client_id} a bien été ajouté à la commande."}
    else:
        request_sql_3 = "SELECT `client`, `order_id` FROM `Order` WHERE order_id = %s;"
        cursor = cursor_bdd(False, request_sql_3, [id])
        id = cursor.fetchone()
        return {"Erreur": "La commande n'a pas été pris en compte",
                "Data": id}

# /order/<id>?delivery=<adress_id> - PUT - body : NON TERMINER
# id: de la commande / adress_id: id de l'adresse
@app.put("/order/{id}?delivery={adress_id}")
def update_delevery_by_client_id(id: int, adress_id: int):
    """ Retourne 200 si ok.
Si un client est associé à la commande et que l'adresse envoyée n'appartient 
pas au client, doit renvoyer 400 """
    request_sql = """
    UPDATE `Order` SET `delevery_adress` = %s
    WHERE `order_id` = %s;"""
    cursor_bdd(True, request_sql, [adress_id, id])
    return {"Delevery": "Mise à jour OK"}

# /order/<id>?billing=<adress_id> - PUT - body :
# id: de la commande / adress_id: id de l'adresse

# Modification du chemin car je ne trouve pas la doc pour le fonctionnement de la query
@app.put("/order/{id}?billing={adress_id}")
def update_biling_by_client_id(id: int, adress_id: int):
    """ Retourne 200 si ok.
Si un client est associé à la commande et que l'adresse envoyée n'appartient 
pas au client, doit renvoyer 400 """
    pass

# /order/<id>?payment=<payment> - PUT - body :
# id: de la commande / payment: le type de payment
@app.put("order/{id}?payment={payment}")
def update_payment_by_client_id(id: int, payment: str):
    """ Retourne 200 si ok """
    pass

# /order/<id>/next - GET - body :
# id: id de la commande
@app.get("/order/{id}/next")
def update_state_by_client_id(id: int, adress_id: int, order: Order):
    """ Change l'état de la commande pour le suivant. 
Si la commande ne répond pas aux éxigences de changement d'état, 
ne pas modifier et renvoyer 400 """
    pass

# ------------------------------------------
# ADRESS --- Création des fonctions API:   |
# ------------------------------------------

#--------------------

#--------------------


# Script:--------------------------------------------------------------------
if __name__ == '__main__':
    pass


# Démarrage de fast API par commande :
# uvicorn Part_2_Bdd_sell:app --reload

# liste = [{"test": 1}, {"coucou": 1}, {"hello world": 1}]
# liste[0] → Affiche le produit
# liste[0]["test"] → Affiche la quantité du produit

# @app.put("/order")
# async def create_order() -> Order:
#     # partie BDD
#     mycursor = mydb.cursor()
#     sql = "insert into cmd values ();"
#     mycursor.execute(sql)
#     mydb.commit()
#     # partie python
#     order = Order()
#     order.id = mycursor.lastrowid
#     return order

# fonction async def ...

    #product.product_id = id
    #desc = product.description

    # Récupère les données de la database
    # request_sql = "SELECT `product_id`, `name`, `description`, `image`, `price`, `available` FROM Product WHERE `product_id` = %s;"
    # cursor = cursor_bdd(False, request_sql, [id])
    # product = cursor.fetchall()

    # Mets à jour la database à partir du Body
    # request_sql = "UPDATE `Product` SET `description`= %s WHERE `product_id` = %s;"
    # cursor = cursor_bdd(True, request_sql, [desc, id])
    # return product

        # Transfert du dico dans product
    # product.product_id = dico_bdd["product_id"]
    # product.name = dico_bdd["name"]
    # setattr(product, 'name', dico_bdd["name"])
    # product.description = desc
    # product.image = dico_bdd["image"]
    # product.price = dico_bdd["price"]
    # product.available = dico_bdd["available"]