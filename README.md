Contexte du projet

A partir du modèle BDD-SELL :

Première partie :

Décrire (à l'écrit) chaque table et ses relations avec les autres tables.

Quelle est la particularité de la relation entre Client et Adress ? A quoi peut elle servir ? Qu'est-ce qu'il y a dans default_adress ?

Refaire un modèle de données en indiquant les types pour une implémentation de ce modèle sur MySQL.

Faire un script init.sql qui crée la base de données.

Deuxième partie :

Créer une API en utilisant ++Fast API++, en suivant le modèle SELL API.

La gestion de l’état de la commande doit suivre le diagramme d’état fourni.

Créer un jeu de tests ++Insomnia++ qui permet de vérifier le fonctionnement de votre API.

Entre autre, le jeu de tests doit (dans l’ordre) :

Créer un produit “Truc”, avec comme description “Lorem Ipsum” comme image “https://picsum.photos/200”, qui coute 200 €.

Créer un produit “Bidule”, avec comme description “Lorem merol” comme image “https://picsum.photos/100”, qui coute 500 €.

Récupérer la liste des produits et vérifier le prix de Truc et de Bidule.

Récupérer Truc à partir de son id et vérifier sa description et son image.

Créer une nouvelle commande.

Ajouter 3 Trucs à la commande.

Ajouter 1 Bidules à la commande.

Créer le client “Bob” avec comme mail “bob@gmail.com”.

Mettre à jour Bob pour ajouter son numéro de téléphone “0123456789”.

Essayer de passer la commande dans l’état suivant (vérifier qu’il y a une erreur).

Associer Bob à la commande

Créer l’adresse “12 street St, 12345 Schenectady, New York, US”

Essayer d’associer cette adresse comme facturation à la commande.

Associer cette adresse à Bob.

Associer cette adresse comme facturation à la commande.

Créer l’adresse “12 boulevard de Strasbourg, 31000, Toulouse, OC, France”

Associer cette adresse à Bob.

Associer cette adresse comme adresse de livraison.

Essayer de passer la commande dans l’état suivant (vérifier qu’il y a une erreur).

Associer le moyen de paiement “Card” à la commande.

Passer la commande dans l’état suivant.

Passer la commande dans l’état suivant.

Vérifier que la commande est dans l’état “sent”.

Récupérer la liste des adresses de Bob.

Récupérer la liste des commandes de Bob.