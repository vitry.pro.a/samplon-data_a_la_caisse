from pydantic import BaseModel


class Client(BaseModel):
    client_id: int | None = None
    name: str | None = None
    email: str | None = None
    phone: str | None = None
    default_adress: int | None = None


class Adress(BaseModel):
    adress_id: int | None = None
    street: str | None = None
    city: str | None = None
    zipcode: int | None = None
    state: str | None = None
    country: str | None = None


class Client_adress(BaseModel):
    client_id: int | None = None
    adress_id: int | None = None


class Order(BaseModel):
    order_id: int  | None = None
    client: int | None = None
    delevery_adress: int | None = None
    biling_adress: int | None = None
    payment_method: str | None = None
    order_state: str | None = "cart"


class Product(BaseModel):
    product_id: int | None = None
    name: str | None = None
    description: str | None = None
    image: str | None = None
    price: int | None = None
    available: bool | None = 0


class Order_lines(BaseModel):
    order_id: int | None = None
    product_id: int | None = None
    qt: int | None = 0


class DictToObject:
    def __init__(self, d):
        if not isinstance(d, dict):
            raise ValueError('The argument d must be a dictionary object')

        for key, value in d.items():
            if isinstance(value, (list, tuple)):
                setattr(self, key, [DictToObject(v) if isinstance(v, dict)
                                    else v for v in value])
            else:
                setattr(self, key, DictToObject(value)
                        if isinstance(value, dict) else value)


if __name__ == '__main__':
    pass