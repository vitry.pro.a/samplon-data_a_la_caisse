-- Active: 1698327840442@@localhost@0@data_at_checkout
USE data_at_checkout;

# ------------------------------------------
# PRODUCT --- Création des requètes SQL:   |
# ------------------------------------------

# /product - GET - Pas de Body: 
--"""Liste des produits en JSON"""
SELECT `product_id`, `name`, `description`, `image`, `price`, `available` FROM Product;

# /product/<id> GET id : id du produit Un produit en JSON
SET id_product = 3;
SELECT `product_id`, `name`, `description`, `image`, `price`, `available` FROM Product
WHERE `product_id` = id_product;

# /product POST body : un produit en JSON Le même produit en JSON avec son id
SET name = Truc2;
--SELECT `product_id`, `name` FROM Product WHERE `name` = name;
INSERT INTO Product (`name`) VALUES (name);

# /product/<id> POST body : un produit en JSON id : id du produit Met à jour le produit
SET colonne = `description`;
SET resultat = "Lorem bestial bis";
SET id_product = 6;
UPDATE `Product` SET colonne = resultat
WHERE `product_id` = id_product;

# /product/<id> DELETE id : id du produit Supprime le produit
SET id_product = 5;
DELETE FROM `Product` 
WHERE `product_id` = id_product;

# ------------------------------------------
# CLIENT --- Création des requètes SQL:    |
# ------------------------------------------

# /client GET Liste des client en JSON
SELECT `client_id`, `name`, `email`, `phone`, `default_adress` FROM `Client`

# /client/<id> GET id : id du client Un client en JSON
SET id_client = 1;
SELECT `client_id`, `name`, `email`, `phone`, `default_adress` FROM `Client`
WHERE `client_id` = id_client;

# /client POST body : un client en JSON Le même client en JSON avec son id
SET name = Fredy;
INSERT INTO Client (`name`) VALUES (name);

# /client/<id> POST body : un client en JSON id : id du client Met à jour le client
SET colonne1 = `phone`;
SET colonne2 = `default_adress`;
SET resultat1 = "0623456789";
SET resultat2 = 1;
SET id_client = 2;
UPDATE `Client` SET `phone` = "0623456789", `default_adress` = 1 WHERE `client_id` = 5;

# /client/<id>/adresses GET id : id du client Retourne la liste des adresses 
# associées à un client
SET id_client = 1;
SELECT `Client`.`name`, `Client_adress`.`client_id`, `Client_adress`.`adress_id`, 
`Adresse`.`street`, `Adresse`.`city`, `Adresse`.`country`, 
`Adresse`.`zipcode`, `Adresse`.`state`
FROM `Client_adress`
JOIN data_at_checkout.`Client` ON `Client`.`client_id` = `Client_adress`.`client_id`
JOIN data_at_checkout.`Adresse` ON `Adresse`.`adress_id` = `Client_adress`.`adress_id`
WHERE `Client_adress`.`client_id` = id_client;

# /client/<id> DELETE id : id du client Supprime le client
SET id_client = 2;
DELETE FROM `Client` 
WHERE `client_id` = id_client;

# ------------------------------------------
# ORDER --- Création des requètes SQL:     |
# ------------------------------------------
SELECT * FROM `Order`;

# /order PUT Crée une nouvelle commande et retourne son id
INSERT INTO `Order` VALUES ();
SELECT LAST_INSERT_ID(); 

DELETE FROM `Order` 
WHERE `order_id` = 5014;

# /order/<id> GET id: id de la commande Retourne la commande 
# avec toutes ses lignes, les adresses et le client.
SET id_client = 5000;

SELECT * FROM `Client`;
SELECT `order_id`, `client`, `delevery_adress`, `biling_adress`, `payment_method`, `order_state` FROM `Order`
WHERE `order_id` = 5000;
SELECT `adress_id`, `street`, `city`, `zipcode`, `state`, `country` FROM `Adresse`
WHERE `adress_id` = 1;
SELECT `order_id`, `product_id`, `qt` FROM `Order_lines`
WHERE `order_id` = 5000;
SELECT `product_id`, `name`, `description`, `image`, `price`, `available` FROM `Product`
WHERE `product_id` = 1;
SELECT `client_id`, `name`, `email`, `phone`, `default_adress` FROM `Client`
WHERE `client_id` = 1;

# /order GET Retourne la liste des commandes
SELECT `order_id`, `client`, `delevery_adress`, `biling_adress`,
`order_state`, `payment_method` FROM `Order`;

# /order/byClient/<id> GET id : id du client 
# Retourne la liste des ids des commandes du client.
SET id_client = 1;
SELECT `client`, `order_id` FROM `Order`
WHERE `client` = 5 and order_id = 5013;

# /order/<id>/<product_id>/<qt> PUT 
# id: id de la commande product_id: id du produit qt: quantité de produit 
# Ajoute ou met à jour une ligne de commande.
SELECT `order_id`, `product_id`, `qt` FROM `Order_lines`
WHERE `order_id` = 5000 and `product_id` = 1;

SET id_product = 1;
SET QT = 2;
SET `id_order` = 5000;
UPDATE `Order_lines` SET `qt` = 10
WHERE `order_id` = 5000 and `product_id` = 1;
INSERT INTO `Order_lines` (`order_id`,`product_id`, `qt`) VALUES (5000, 1, 1)

# /order/<id>/<client_id> PUT id: id de la commande client_id: id du client
# Assigne une commande à un client. 
# Attention ! une fois qu'une commande est attribuée à un client on ne peut plus modifier cette relation.
SET id_order = 5008;
SET id_client = 1;
UPDATE `Order` SET `client` = id_client
WHERE `order_id` = id_order;

# /order/<id>?delivery=<adress_id> PUT id: de la commande adress_id: id de l'adresse.
# Retourne 200 si ok.
# Si un client est associé à la commande et que l'adresse envoyée n'appartient pas au client, doit renvoyer 400.
SET id_order = 5008;
SET id_adress = 3;
UPDATE `Order` SET `delevery_adress` = id_adress
WHERE `order_id` = id_order;

# /order/<id>?billing=<adress_id> PUT id: de la commande adress_id: id de l'adresse.
# Retourne 200 si ok.
# Si un client est associé à la commande et que l'adresse envoyée n'appartient pas au client, doit renvoyer 400.
SET id_order = 5008;
SET id_adress = 3;
UPDATE `Order` SET `biling_adress` = id_adress
WHERE `order_id` = id_order;

# /order/<id>?payment=<payment> PUT id: de la commande payment: le type de payment Retourne 200 si ok.
SET id_order = 5008;
SET method_payment = "credit_card";
UPDATE `Order` SET `payment_method` = method_payment
WHERE `order_id` = id_order;

# /order/<id>/next GET id: id de la commande 
# Change l'état de la commande pour le suivant. Si la commande ne répond pas aux éxigences de changement d'état, 
# ne pas modifier et renvoyer 400.
SET id_order = 5008;
SET state_order = "validated";
UPDATE `Order` SET `order_state` = state_order
WHERE `order_id` = id_order;

# ------------------------------------------
# ADRESS --- Création des requètes SQL:    |
# ------------------------------------------

# /adress POST body: l'adresse en JSON Retourne l'id de l'adresse créer. Si une adresse identique existe, retourne l'id 
# de l'adresse déjà existante
SET street = "123 street St";
SET city = "Toulouse";
SET zipcode = "31000";
SET str_state = "OC";
SET country = "France";
INSERT INTO `Adresse` (`street`, `city`, `zipcode`, `state`, `country`)
VALUES (street, city, zipcode, str_state, country);

# ---------------------------------------------------------------------
# Zone de test:
SELECT * FROM Product;

# Créer un produit “Truc”, avec comme description “Lorem Ipsum” comme 
# image “https://picsum.photos/200”, qui coute 200 €.
INSERT INTO `Product` (`name`, `description`, `image`, `price`) 
VALUES("Truc", "Lorem Ipsum", "https://picsum.photos/200", 200);

INSERT INTO `Product` (`name`, `description`, `image`, `price`) 
VALUES("Chose", "Lorem bestial", "https://picsum.photos/300", 450);

# Créer un produit “Bidule”, avec comme description “Lorem merol” comme 
# image “https://picsum.photos/100”, qui coute 500 €.
INSERT INTO `Product` (`name`, `description`, `image`, `price`) 
VALUES("Bidule", "Lorem merol", "https://picsum.photos/100", 500);

# Récupérer la liste des produits et vérifier le prix de Truc et de Bidule.
SELECT `product_id`, `name`, `description`, `image`, `price`, `available` FROM Product;
SELECT `price` FROM Product
WHERE `name` = "Truc";
SELECT `price` FROM Product
WHERE `name` = "Bidule";

# Récupérer Truc à partir de son id et vérifier sa description et son image.
SELECT `description`, `image` FROM Product
WHERE `product_id` in (SELECT `product_id` FROM Product WHERE `name` = "Truc");


SELECT * FROM `Order`;

# Créer une nouvelle commande.
INSERT INTO `Order` 
VALUES();

SELECT * FROM `Order_lines`;

# Ajouter 3 Trucs à la commande.
INSERT INTO `Order_lines` (`order_id`, `product_id`, `qt`) 
VALUES(5000, 1, 3);

# Ajouter 1 Bidules à la commande.
INSERT INTO `Order_lines` (`order_id`, `product_id`, `qt`) 
VALUES(5000, 2, 1);


SELECT * FROM `Client`;

# Créer le client “Bob” avec comme mail “bob@gmail.com”.
INSERT INTO `Client` (`name`, `email`) 
VALUES("Bob", "bob@gmail.com");

INSERT INTO `Client` (`name`, `email`) 
VALUES("Fred", "fred@gmail.com");

# Mettre à jour Bob pour ajouter son numéro de téléphone “0123456789”.
UPDATE `Client` SET `phone` = "0123456789"
WHERE `name` = "Bob";

UPDATE `Client` SET `phone` = "0123456789"
WHERE `name` = "Fred";

SELECT * FROM `Order`;
SELECT * FROM `Client`;

# Essayer de passer la commande dans l’état suivant (vérifier qu’il y a une erreur).
# Erreur uniquement en mode requête Http.
UPDATE `Order` SET `order_state` = "validated"
WHERE `order_id` = 5000;

# Associer Bob à la commande
UPDATE `Order` SET `client` = 1
WHERE `order_id` = 5000;

UPDATE `Order` SET `client` = 2
WHERE `order_id` = 5001;


SELECT * FROM `Adresse`;
SELECT * FROM `Order`;
SELECT * FROM `Client`;
SELECT * FROM `Client_adress`

# Créer l’adresse “12 street St, 12345 Schenectady, New York, US”
INSERT INTO `Adresse` (`street`, `city`, `zipcode`, `state`, `country`) 
VALUES("12 street St", "Schenectady", 12345, "New York", "US");

# Essayer d’associer cette adresse comme facturation à la commande.
UPDATE `Order` SET `biling_adress` = 1
WHERE `order_id` = 5000;

# Associer cette adresse à Bob.
INSERT INTO `Client_adress` (`client_id`, `adress_id`)
VALUES(1, 1)

UPDATE `Client` SET `default_adress` = 2
WHERE `name` = "Bob";

INSERT INTO `Client_adress` (`client_id`, `adress_id`)
VALUES(2, 1)

UPDATE `Client` SET `default_adress` = 1
WHERE `name` = "Fred";

# Associer cette adresse comme facturation à la commande.
UPDATE `Order` SET `delevery_adress` = 1
WHERE `order_id` = 5000;


SELECT * FROM `Adresse`;
SELECT * FROM `Order`;
SELECT * FROM `Client`;
SELECT * FROM `Client_adress`

# Créer l’adresse “12 boulevard de Strasbourg, 31000, Toulouse, OC, France”
INSERT INTO `Adresse` (`street`, `city`, `zipcode`, `state`, `country`) 
VALUES("12 boulevard de Strasbourg", "Toulouse", 31000, "OC ", "France");

INSERT INTO `Adresse` (`street`, `city`, `zipcode`, `state`, `country`) 
VALUES("3 rue claude cornac", "Toulouse", 31140, "OC ", "France");

# Associer cette adresse à Bob.
INSERT INTO `Client_adress` (`client_id`, `adress_id`)
VALUES(1, 2)

# Associer cette adresse comme adresse de livraison.
UPDATE `Order` SET `delevery_adress` = 2
WHERE `order_id` = 5000;

# Essayer de passer la commande dans l’état suivant (vérifier qu’il y a une erreur).
UPDATE `Order` SET `order_state` = "validated"
WHERE `order_id` = 5000;

# Associer le moyen de paiement “Card” à la commande.
UPDATE `Order` SET `payment_method` = "credit_card"
WHERE `order_id` = 5000;

# Passer la commande dans l’état suivant.
UPDATE `Order` SET `order_state` = "validated"
WHERE `order_id` = 5000;

# Passer la commande dans l’état suivant.
UPDATE `Order` SET `order_state` = "sent"
WHERE `order_id` = 5000;

# Vérifier que la commande est dans l’état “sent”.
SELECT `order_state` FROM `Order`
WHERE `order_id` = 5000;

# Récupérer la liste des adresses de Bob.
SELECT `Client`.`name`, `Client_adress`.`client_id`, `Client_adress`.`adress_id`, 
`Adresse`.`street`, `Adresse`.`city`, `Adresse`.`country`, 
`Adresse`.`zipcode`, `Adresse`.`state`
FROM `Client_adress`
JOIN data_at_checkout.`Client` ON `Client`.`client_id` = `Client_adress`.`client_id`
JOIN data_at_checkout.`Adresse` ON `Adresse`.`adress_id` = `Client_adress`.`adress_id`
WHERE `Client_adress`.`client_id` IN (
    SELECT `client_id` FROM `Client`
    WHERE `name` = "Bob");


# Récupérer la liste des commandes de Bob.
SELECT `Order`.`order_id`, `Client`.`name` FROM `Order`
JOIN data_at_checkout.`Client` ON `Client`.`client_id` = `Order`.`client`
WHERE `Client`.`client_id` IN (
    SELECT `client_id` FROM `Client`
    WHERE `name` = "Bob");