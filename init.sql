DROP DATABASE IF EXISTS data_at_checkout;
-- Active: 1698327840442@@localhost@0
CREATE DATABASE data_at_checkout
  DEFAULT CHARACTER SET = 'utf8mb4';
USE data_at_checkout;

CREATE USER 'antony'@'localhost' IDENTIFIED BY 'choupette';
GRANT ALL PRIVILEGES ON . TO 'antony'@'localhost';

DROP TABLE IF EXISTS `Client`;
CREATE TABLE `Client` (
  `client_id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'Nom-prénom',
  `email` varchar(255) NOT NULL,
  `phone` varchar(255),
  `default_adress` int
);

DROP TABLE IF EXISTS `Adress`;
CREATE TABLE `Adress` (
  `adress_id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `street` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `zipcode` int NOT NULL,
  `state` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL
);

DROP TABLE IF EXISTS `Client_adress`;
CREATE TABLE `Client_adress` (
  `client_id` int,
  `adress_id` int
);

DROP TABLE IF EXISTS `Order`;
CREATE TABLE `Order` (
  `order_id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `client` int,
  `delevery_adress` int,
  `biling_adress` int,
  `payment_method` ENUM ('credit_card', 'bank_deck', 'cash_on_delivery'),
  `order_state` ENUM ('cart', 'validated', 'sent', 'delivered') DEFAULT "cart"
);

DROP TABLE IF EXISTS `Product`;
CREATE TABLE `Product` (
  `product_id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255),
  `image` varchar(255),
  `price` int NOT NULL,
  `available` boolean DEFAULT 0
);

DROP TABLE IF EXISTS `Order_lines`;
CREATE TABLE `Order_lines` (
  `order_id` int NOT NULL,
  `product_id` int NOT NULL,
  `qt` int DEFAULT 0
);

ALTER TABLE `Client_adress` ADD FOREIGN KEY (`client_id`) REFERENCES `Client` (`client_id`);

ALTER TABLE `Client_adress` ADD FOREIGN KEY (`adress_id`) REFERENCES `Adresse` (`adress_id`);

ALTER TABLE `Order_lines` ADD FOREIGN KEY (`product_id`) REFERENCES `Product` (`product_id`);

ALTER TABLE `Order_lines` ADD FOREIGN KEY (`order_id`) REFERENCES `Order` (`order_id`);

ALTER TABLE `Order` ADD FOREIGN KEY (`delevery_adress`) REFERENCES `Adresse` (`adress_id`);

ALTER TABLE `Order` ADD FOREIGN KEY (`biling_adress`) REFERENCES `Adresse` (`adress_id`);

ALTER TABLE `Order` ADD FOREIGN KEY (`client`) REFERENCES `Client` (`client_id`);

ALTER TABLE `Client` ADD FOREIGN KEY (`default_adress`) REFERENCES `Adresse` (`adress_id`);

