### Met à jour la liste de paquet
```sh
sudo apt update
```

### Install MySQL
```sh
sudo apt install mysql-server
```

### Démarre mySQL via un fichier
```sh
sudo mysql < nom_du_fichier.sql
```

### Démarre mySQL
```sh
sudo systenctl start mysql
sudo mysql
```

### Afficher les Databases
```sql
SHOW DATABASE;
```

### Afficher la Database
```sql
SHOW nom_de_la_database;
```

### Créer une Database
```sql
CREATE DATABASE;
```

### Sélectionner une Database
```sql 
USE nom_de_la_database;
```

### Afficher les tables
```sql
SHOW TABLES;
```

### Afficher la table
```sql
SHOW nom_de_la_table;
```

### Afficher le contenu de la table
```sql
DESCRIBE nom_de_la_table;
```
## Sortir de mysql
```sql
exit;
```